package com.example.pesajeapp.fragments;


import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.pesajeapp.R;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Galpon;
import com.example.pesajeapp.model.Resumen;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResumenMachoFragment extends Fragment {


  private TextView tvCantidadAves;
  private TextView tvCantidadJabas;
  private TextView tvPesoTotalTara;
  private TextView tvPesoBruto;
  private TextView tvPesoNeto;
  private TextView tvPesoPromedio;
  private TextView tvGalpon;
  private TextView tvJabasAves;
  private TextView tvPollosJaba;
  private TextView tvCodigoBalanza;
  private Resumen resumen;
  List<Galpon> galpones;
  String pedidoId;
  int jabaPesada;
  int avesJaba;

  public ResumenMachoFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_resumen_macho, container, false);
    pedidoId = getArguments().getString("pedidoId");

    jabaPesada = getArguments().getInt("jaba_pesada");
    avesJaba = getArguments().getInt("aves_jaba");

    GetResumen resumen = new GetResumen();
    resumen.execute();

    GetGalpones galpones = new GetGalpones();
    galpones.execute();

    initComponents(view);

    return view;

  }

  private void initComponents(View view) {
    tvCantidadAves = view.findViewById(R.id.tv_cantidad_aves);
    tvCantidadJabas = view.findViewById(R.id.tv_cantidad_jabas);
    tvPesoTotalTara = view.findViewById(R.id.tv_peso_total_tara);
    tvPesoBruto = view.findViewById(R.id.tv_peso_bruto);
    tvPesoNeto = view.findViewById(R.id.tv_peso_neto);
    tvPesoPromedio = view.findViewById(R.id.tv_peso_promedio);
    tvGalpon = view.findViewById(R.id.tv_galpon);
    tvJabasAves = view.findViewById(R.id.tv_jabas_aves);
    tvPollosJaba = view.findViewById(R.id.tv_pollos_jaba);
    tvCodigoBalanza = view.findViewById(R.id.tv_codigo_balanza);
  }

  @SuppressLint("DefaultLocale")
  private void setData(Resumen resumen) {
    tvCantidadAves.setText(String.valueOf(resumen.getCantidad_aves()));
    tvCantidadJabas.setText(String.valueOf(resumen.getCantidad_jabas()));
    tvPesoTotalTara.setText(String.valueOf(resumen.getTotal_peso_taras()));
    tvPesoBruto.setText(String.valueOf(resumen.getPeso_bruto()));
    tvPesoNeto.setText(String.valueOf(resumen.getPeso_neto()));
    tvPesoPromedio.setText(String.format("%.2f", resumen.getPeso_promedio()));
    tvPollosJaba.setText(String.format("%d x %d", jabaPesada, avesJaba));
    tvJabasAves.setText(String.format("PEDIDO %d x %d", jabaPesada, avesJaba));
    tvGalpon.setText(setGalpones());
    tvCodigoBalanza.setText(resumen.getCodigo_balanza());
  }

  private String setGalpones() {
    StringBuilder galponesString = new StringBuilder();

    for (Galpon galpon : galpones) {
      galponesString.append(galpon).append("   ");
    }

    return galponesString.toString().trim();
  }

  private class GetResumen extends AsyncTask<Void, Void, Resumen> {

    @Override
    protected Resumen doInBackground(Void... voids) {
      resumen = PesajeDatabse.getInstance(getContext()).pedidoStore().getResumenMacho(pedidoId, "M");

      return resumen;
    }

    @Override
    protected void onPostExecute(Resumen resumen) {
      setData(resumen);
      Log.d("RESUMEN", resumen.toString());
    }
  }

  private class GetGalpones extends AsyncTask<Void, Void, List<Galpon>> {

    @Override
    protected List<Galpon> doInBackground(Void... voids) {
      galpones =
          PesajeDatabse.getInstance(getContext()).pedidoStore().getGalpones(pedidoId,
              "M");
      return galpones;
    }

    @Override
    protected void onPostExecute(List<Galpon> galpones) {
      Log.d("RESUMEN", galpones.toString());
    }
  }
}
