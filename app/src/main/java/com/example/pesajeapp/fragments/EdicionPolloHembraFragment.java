package com.example.pesajeapp.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pesajeapp.R;
import com.example.pesajeapp.activities.PesajeAveActivity;
import com.example.pesajeapp.activities.PesajeTaraActivity;
import com.example.pesajeapp.activities.ResumenMachoActivity;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pesada;

import java.util.ArrayList;
import java.util.List;

public class EdicionPolloHembraFragment extends Fragment {


  Spinner spnGalponesHembra;
  Spinner spnJabasXPesadaHembra;
  Spinner spnAvesXJabaHembra;
  Spinner spnCorralHembra;
  Button btnPesajeTarasHembra;
  Button btnResumenHembra;
  Button btnGuardarPollosHembra;
  Button btnPesajeAvesHembra;
  TextView tvCantidadHembras;

  int jabas_pesada;
  int aves_jaba;
  double total_jabas;
  int cantidad_pesadas;
  int cantidad_hembras;
  int galpon;
  boolean configurado;
  boolean estaFinalizadaTara;
  boolean estaFinalizadaAve;

  String pedidoId;
  List<Pesada> pesadas;
  Pesada pesada;

  public EdicionPolloHembraFragment() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_edicion_pollo_hembra, container, false);
    pedidoId = getArguments().getString("pedidoId");
    configurado = getArguments().getBoolean("configurado_hembra");
    estaFinalizadaTara = getArguments().getBoolean("tara_finalizada_hembra");
    estaFinalizadaAve = getArguments().getBoolean("ave_finalizada");

    initComponents(view);
    initUI();
    return view;
  }

  private void initComponents(View view) {
    spnGalponesHembra = view.findViewById(R.id.spn_galpones_hembra);
    spnJabasXPesadaHembra = view.findViewById(R.id.spn_jabas_pesada_hembra);
    spnAvesXJabaHembra = view.findViewById(R.id.spn_aves_jaba_hembra);
    spnCorralHembra = view.findViewById(R.id.spn_corral_hembra);

    btnPesajeTarasHembra = view.findViewById(R.id.btn_pesaje_taras_hembra);
    btnResumenHembra = view.findViewById(R.id.btn_resumen_hembra);
    btnGuardarPollosHembra = view.findViewById(R.id.btn_guardar_pollos_hembra);
    btnPesajeAvesHembra = view.findViewById(R.id.btn_pesaje_aves_hembra);

    tvCantidadHembras = view.findViewById(R.id.tv_cantidad_hembras);
  }

  private void initUI() {
    setupSpinners();
    setupButtons();
    estaConfigurado();
  }

  private void estaConfigurado() {

    Log.d("TARAFINALIZADA", String.valueOf(estaFinalizadaTara));

    if (configurado) {
      tvCantidadHembras.setEnabled(false);
      spnGalponesHembra.setEnabled(false);
      spnCorralHembra.setEnabled(false);
      spnJabasXPesadaHembra.setEnabled(false);
      spnAvesXJabaHembra.setEnabled(false);
      btnGuardarPollosHembra.setEnabled(false);
      btnPesajeTarasHembra.setEnabled(true);
    }

    if(estaFinalizadaTara) {
      tvCantidadHembras.setEnabled(false);
      spnGalponesHembra.setEnabled(false);
      spnCorralHembra.setEnabled(false);
      spnJabasXPesadaHembra.setEnabled(false);
      spnAvesXJabaHembra.setEnabled(false);
      btnPesajeTarasHembra.setEnabled(false);
      btnGuardarPollosHembra.setEnabled(false);
      btnPesajeAvesHembra.setEnabled(true);
    }

    if (estaFinalizadaAve){
      tvCantidadHembras.setEnabled(false);
      spnGalponesHembra.setEnabled(false);
      spnCorralHembra.setEnabled(false);
      spnJabasXPesadaHembra.setEnabled(false);
      spnAvesXJabaHembra.setEnabled(false);
      btnPesajeAvesHembra.setEnabled(false);
      btnResumenHembra.setEnabled(true);
    }
  }

  private void setupSpinners() {
    List<String> galpones = setCantidadGalpones();
    List<String> jabas = setCantidadJabasXPesada();
    List<String> aves = setCantidadAvesXJaba();
    List<String> corrales = setCantidadCorral();

    ArrayAdapter<String> galponesAdapter = new ArrayAdapter<>(getContext(),
        android.R.layout.simple_spinner_item, galpones);
    ArrayAdapter<String> jabasAdapter = new ArrayAdapter<>(getContext(),
        android.R.layout.simple_spinner_item, jabas);
    ArrayAdapter<String> avesAdapter = new ArrayAdapter<>(getContext(),
        android.R.layout.simple_spinner_item, aves);
    ArrayAdapter<String> corralesAdapter = new ArrayAdapter<>(getContext(),
        android.R.layout.simple_spinner_item, corrales);

    galponesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    jabasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    avesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    spnGalponesHembra.setAdapter(galponesAdapter);
    spnGalponesHembra.setSelection(9);

    spnJabasXPesadaHembra.setAdapter(galponesAdapter);
    spnJabasXPesadaHembra.setSelection(9);

    spnAvesXJabaHembra.setAdapter(avesAdapter);
    spnAvesXJabaHembra.setSelection(7);

    spnCorralHembra.setAdapter(corralesAdapter);
    spnCorralHembra.setSelection(7);

  }

  private void setupButtons() {
    btnGuardarPollosHembra.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        aves_jaba = Integer.parseInt(spnAvesXJabaHembra.getSelectedItem().toString());
        jabas_pesada = Integer.parseInt(spnJabasXPesadaHembra.getSelectedItem().toString());
        galpon = Integer.parseInt(spnGalponesHembra.getSelectedItem().toString());
        cantidad_hembras = Integer.parseInt(tvCantidadHembras.getText().toString());

        total_jabas = Math.ceil((double) cantidad_hembras / aves_jaba);
        cantidad_pesadas = (int) Math.ceil(total_jabas / jabas_pesada);

        pesada = new Pesada();
        pesada.setAves_por_jaba(aves_jaba);
        pesada.setJabas_por_pesada(jabas_pesada);
        pesada.setGalpon(String.valueOf(galpon));
        pesada.setSexo("F");
        pesada.setPedidoId(pedidoId);
        pesada.setTara_peso(0);
        pesada.setTara_pesada(false);
        pesada.setAve_peso(0);
        pesada.setAve_pesada(false);
        pesada.setCodigo_balanza("");
        pesada.setImprimible(false);
        pesada.setFinalizado(false);
        configPesada(pesada, cantidad_pesadas);

      }
    });

    btnPesajeTarasHembra.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(getActivity(), PesajeTaraActivity.class)
            .putExtra("pedidoId", pedidoId)
            .putExtra("sexo", "F")
            .putExtra("tara_finalizada", estaFinalizadaTara));
      }
    });

    btnResumenHembra.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(getActivity(), ResumenMachoActivity.class));
      }
    });

    btnPesajeAvesHembra.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getActivity(), PesajeAveActivity.class).putExtra("pedidoId",
            pedidoId));
      }
    });
  }

  private void configPesada(Pesada pesada, int cantidad_pesadas) {

    for (int i = 1; i <= cantidad_pesadas; i++) {
      PesajeDatabse.getInstance(getContext()).pesadaStore().insertPesada(pesada);
    }

    PesajeDatabse.getInstance(getContext()).pedidoStore().updatePedidoConfiguradoHembra(pedidoId);

    Intent intent = getActivity().getIntent();
    getActivity().finish();
    startActivity(intent);
  }

  private List<String> setCantidadGalpones() {
    List<String> galpones = new ArrayList<>();

    for (int i = 1; i <= 25; i++) {
      galpones.add("" + i + "");
    }

    return galpones;
  }

  private List<String> setCantidadJabasXPesada() {
    List<String> jabas = new ArrayList<>();

    for (int i = 1; i <= 15; i++) {
      jabas.add("" + i + "");
    }
    return jabas;
  }

  private List<String> setCantidadAvesXJaba() {
    List<String> aves = new ArrayList<>();

    for (int i = 1; i <= 12; i++) {
      aves.add("" + i + "");
    }
    return aves;
  }


  private List<String> setCantidadCorral() {
    List<String> corrales = new ArrayList<>();

    for (int i = 1; i <= 12; i++) {
      corrales.add("" + i + "");
    }
    return corrales;
  }
}
