package com.example.pesajeapp.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.adapters.PedidosAdapter;
import com.example.pesajeapp.model.Pedido;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PesajeTarasFragment extends Fragment {

  private RecyclerView recyclerView;
  private PedidosAdapter pedidosAdapter;

  public PesajeTarasFragment() {
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_pesaje_taras, container, false);

    recyclerView = view.findViewById(R.id.rv_pedido);

    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    pedidosAdapter = new PedidosAdapter(pedidos(), new PedidosAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(Pedido pedido) {
        Toast.makeText(getContext(), pedido.getNombre_cliente(), Toast.LENGTH_SHORT).show();
      }
    });

    recyclerView.setAdapter(pedidosAdapter);

    return view;
  }

  private List<Pedido> pedidos() {
    List<Pedido> pedidos = new ArrayList<>();

    for (int i = 1; i <= 8; i++) {
      Pedido pedido = new Pedido();
      pedido.setNombre_cliente("Cliente: " + i);
      pedido.setRuc_cliente("RUC: " + i * 1111111);
      pedido.setTipo_pedido("Granja");
      pedido.setFecha_pedido(new Date().toLocaleString());
      pedidos.add(pedido);
    }

    return pedidos;
  }



}
