package com.example.pesajeapp.fragments;


import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.adapters.TaraResumenAdapter;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pesada;

import java.util.List;

public class ResumenTarasMachoFragment extends Fragment {

  private RecyclerView recyclerView;
  private TaraResumenAdapter taraResumenAdapter;
  String pedidoId;
  String sexo;
  List<Pesada> pesadas;
  TextView tvCantidadTarasPesadas;
  TextView tvJabaAves;
  int cantidadPesadas;
  int jabaPesada;
  int avesJaba;

  public ResumenTarasMachoFragment() {

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_resumen_taras_macho, container, false);

    pedidoId = getArguments().getString("pedidoId");
    sexo = getArguments().getString("sexo");
    cantidadPesadas = getArguments().getInt("cantidad_pesadas");
    jabaPesada = getArguments().getInt("jaba_pesada");
    avesJaba = getArguments().getInt("aves_jaba");

    initComponents(view);

    SelectPesadas selectPesadas = new SelectPesadas();
    selectPesadas.execute();

    initUI();
    return view;
  }

  private void initComponents(View view) {
    recyclerView = view.findViewById(R.id.rv_resumen_brutos_macho);
    tvJabaAves = view.findViewById(R.id.tv_jabas_aves);
    tvCantidadTarasPesadas = view.findViewById(R.id.tv_cantidad_taras_pesadas);
  }

  @SuppressLint("DefaultLocale")
  private void initUI() {
    tvCantidadTarasPesadas.setText(String.format("TARAS PESADAS %d", cantidadPesadas));
    tvJabaAves.setText(String.format("PEDIDO %d x %d", jabaPesada, avesJaba));
  }

  private void setupRecyclerView(List<Pesada> pesadas) {

    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    taraResumenAdapter = new TaraResumenAdapter(pesadas);

    recyclerView.setAdapter(taraResumenAdapter);
  }

  private class SelectPesadas extends AsyncTask<Void, Void, List<Pesada>> {

    @Override
    protected List<Pesada> doInBackground(Void... voids) {
      pesadas = PesajeDatabse.getInstance(getContext()).pesadaStore().getPesadas(pedidoId, "M");
      return pesadas;
    }

    @Override
    protected void onPostExecute(List<Pesada> pesadas) {
      setupRecyclerView(pesadas);
    }
  }

}
