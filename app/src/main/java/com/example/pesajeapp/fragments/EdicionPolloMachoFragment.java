package com.example.pesajeapp.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.pesajeapp.R;
import com.example.pesajeapp.activities.PesajeAveActivity;
import com.example.pesajeapp.activities.PesajeTaraActivity;
import com.example.pesajeapp.activities.PesajeTarasMachoListaActivity;
import com.example.pesajeapp.activities.ResumenMachoActivity;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pesada;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EdicionPolloMachoFragment extends Fragment {


  Spinner spnGalponesMacho;
  Spinner spnJabasXPesadaMacho;
  Spinner spnAvesXJabaMacho;
  Spinner spnCorralMacho;
  Button btnPesajeTarasMacho;
  Button btnResumenMacho;
  Button btnGuardarPollosMacho;
  Button btnPesajeAvesMacho;
  TextView tvCantidadMachos;

  int jabas_pesada;
  int aves_jaba;
  double total_jabas;
  int cantidad_pesadas;
  int cantidad_machos;
  int galpon;
  boolean configurado;
  boolean estaFinalizadaTara;
  boolean estaFinalizadaAve;

  String pedidoId;
  List<Pesada> pesadas;
  Pesada pesada;

  public EdicionPolloMachoFragment() {
    // Required empty public constructor
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_edicion_pollo_macho, container, false);
    pedidoId = getArguments().getString("pedidoId");
    configurado = getArguments().getBoolean("configurado_macho");
    estaFinalizadaTara = getArguments().getBoolean("tara_finalizada_macho");
    estaFinalizadaAve = getArguments().getBoolean("ave_finalizada");
    initComponents(view);
    initUI();
    return view;
  }

  private void initComponents(View view) {
    spnGalponesMacho = view.findViewById(R.id.spn_galpones);
    spnJabasXPesadaMacho = view.findViewById(R.id.tv_jabas_pesada);
    spnAvesXJabaMacho = view.findViewById(R.id.tv_aves_jaba);
    spnCorralMacho = view.findViewById(R.id.spn_corral);

    btnPesajeTarasMacho = view.findViewById(R.id.btn_pesaje_taras_macho);
    btnResumenMacho = view.findViewById(R.id.btn_resumen_macho);
    btnGuardarPollosMacho = view.findViewById(R.id.btn_guardar_pollos_macho);
    btnPesajeAvesMacho = view.findViewById(R.id.btn_pesaje_aves_macho);

    tvCantidadMachos = view.findViewById(R.id.tv_cantidad_machos);
  }

  private void initUI() {
    setupSpinners();
    setupButtons();
    estaConfigurado();
  }

  private void estaConfigurado() {
    if (configurado) {
      spnGalponesMacho.setEnabled(false);
      spnCorralMacho.setEnabled(false);
      spnJabasXPesadaMacho.setEnabled(false);
      spnAvesXJabaMacho.setEnabled(false);
      tvCantidadMachos.setEnabled(false);
      btnGuardarPollosMacho.setEnabled(false);
      btnPesajeTarasMacho.setEnabled(true);
    }

    if (estaFinalizadaTara) {
      spnGalponesMacho.setEnabled(false);
      spnCorralMacho.setEnabled(false);
      spnJabasXPesadaMacho.setEnabled(false);
      spnAvesXJabaMacho.setEnabled(false);
      tvCantidadMachos.setEnabled(false);
      btnPesajeTarasMacho.setEnabled(false);
      btnGuardarPollosMacho.setEnabled(false);
      btnPesajeAvesMacho.setEnabled(true);
    }

    if (estaFinalizadaAve) {
      spnGalponesMacho.setEnabled(false);
      spnCorralMacho.setEnabled(false);
      spnJabasXPesadaMacho.setEnabled(false);
      spnAvesXJabaMacho.setEnabled(false);
      tvCantidadMachos.setEnabled(false);
      btnPesajeAvesMacho.setEnabled(false);
      btnResumenMacho.setEnabled(true);
    }

  }

  private void setupSpinners() {
    List<String> galpones = setCantidadGalpones();
    List<String> jabas = setCantidadJabasXPesada();
    List<String> aves = setCantidadAvesXJaba();
    List<String> corrales = setCantidadCorral();

    ArrayAdapter<String> galponesAdapter = new ArrayAdapter<>(Objects.requireNonNull(getContext()),
        android.R.layout.simple_spinner_item, galpones);
    ArrayAdapter<String> jabasAdapter = new ArrayAdapter<>(getContext(),
        android.R.layout.simple_spinner_item, jabas);
    ArrayAdapter<String> avesAdapter = new ArrayAdapter<>(getContext(),
        android.R.layout.simple_spinner_item, aves);
    ArrayAdapter<String> corralesAdapter = new ArrayAdapter<>(getContext(),
        android.R.layout.simple_spinner_item, corrales);

    galponesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    jabasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    avesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    corralesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    spnGalponesMacho.setAdapter(galponesAdapter);
    spnGalponesMacho.setSelection(9);

    spnJabasXPesadaMacho.setAdapter(jabasAdapter);
    spnJabasXPesadaMacho.setSelection(9);

    spnAvesXJabaMacho.setAdapter(avesAdapter);
    spnAvesXJabaMacho.setSelection(6);

    spnCorralMacho.setAdapter(corralesAdapter);
    spnCorralMacho.setSelection(7);

  }

  private void setupButtons() {
    btnGuardarPollosMacho.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        aves_jaba = Integer.parseInt(spnAvesXJabaMacho.getSelectedItem().toString());
        jabas_pesada = Integer.parseInt(spnJabasXPesadaMacho.getSelectedItem().toString());
        galpon = Integer.parseInt(spnGalponesMacho.getSelectedItem().toString());
        cantidad_machos = Integer.parseInt(tvCantidadMachos.getText().toString());

        total_jabas = Math.ceil((double) cantidad_machos / aves_jaba);
        cantidad_pesadas = (int) Math.ceil(total_jabas / jabas_pesada);

        pesada = new Pesada();
        pesada.setAves_por_jaba(aves_jaba);
        pesada.setJabas_por_pesada(jabas_pesada);
        pesada.setGalpon(String.valueOf(galpon));
        pesada.setSexo("M");
        pesada.setPedidoId(pedidoId);
        pesada.setTara_peso(0);
        pesada.setTara_pesada(false);
        pesada.setAve_peso(0);
        pesada.setAve_pesada(false);
        pesada.setCodigo_balanza("");
        pesada.setImprimible(false);
        pesada.setFinalizado(false);
        configPesada(pesada, cantidad_pesadas);

      }
    });

    btnPesajeTarasMacho.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(getActivity(), PesajeTaraActivity.class)
            .putExtra("pedidoId", pedidoId)
            .putExtra("sexo", "M")
            .putExtra("tara_finalizada", estaFinalizadaTara));
      }
    });

    btnResumenMacho.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        startActivity(new Intent(getActivity(), ResumenMachoActivity.class)
            .putExtra("pedidoId", pedidoId)
            .putExtra("sexo", "M"));
      }
    });

    btnPesajeAvesMacho.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getActivity(), PesajeAveActivity.class).putExtra("pedidoId",
            pedidoId));
      }
    });
  }

  private void configPesada(Pesada pesada, int cantidad_pesadas) {

    for (int i = 1; i <= cantidad_pesadas; i++) {
      PesajeDatabse.getInstance(getContext()).pesadaStore().insertPesada(pesada);
    }

    PesajeDatabse.getInstance(getContext()).pedidoStore().updatePedidoConfiguradoMacho(pedidoId);


    Intent intent = Objects.requireNonNull(getActivity()).getIntent();
    getActivity().finish();
    startActivity(intent);
  }

  private List<String> setCantidadGalpones() {
    List<String> galpones = new ArrayList<>();

    for (int i = 1; i <= 25; i++) {
      galpones.add("" + i + "");
    }

    return galpones;
  }

  private List<String> setCantidadJabasXPesada() {
    List<String> jabas = new ArrayList<>();

    for (int i = 1; i <= 15; i++) {
      jabas.add("" + i + "");
    }
    return jabas;
  }

  private List<String> setCantidadAvesXJaba() {
    List<String> aves = new ArrayList<>();

    for (int i = 1; i <= 12; i++) {
      aves.add("" + i + "");
    }
    return aves;
  }

  private List<String> setCantidadCorral() {
    List<String> corrales = new ArrayList<>();

    for (int i = 1; i <= 12; i++) {
      corrales.add("" + i + "");
    }
    return corrales;
  }

}
