package com.example.pesajeapp.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.pesajeapp.R;
import com.example.pesajeapp.activities.PesajeTarasHembraListaActivity;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pesada;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PesajeAveHembraFragment extends Fragment {

  TextView tvPesadaId;
  Spinner spnGalpones;
  TextView tvJabasPesada;
  TextView tvAvesJaba;
  EditText etCodigoBalanza;
  EditText etPesoAve;
  Button button2;
  Button btnGuardarPeso;
  Button btnListarAvesPesada;
  Button btnCerrarPesadaAves;
  String pedidoId;
  List<Pesada> pesadas;
  int posicion;

  Boolean aveFinalizada;

  public PesajeAveHembraFragment() {
    // Required empty public constructor
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_pesaje_ave, container, false);

    pedidoId = getArguments().getString("pedidoId");
    aveFinalizada = getArguments().getBoolean("ave_finalizada");

    initViews(view);
    getData();
    initUI();
    return view;
  }

  private void getData() {
    GetPesadas getPesadas = new GetPesadas();
    getPesadas.execute();
  }

  private void calcularPosicion(List<Pesada> pesadas) {
    posicion = pesadas.size();
    for (int i = 1; i <= pesadas.size(); i++) {
      if (!pesadas.get(i - 1).isAve_pesada()) {
        posicion = i;
        break;
      }
    }
    setData(posicion);
  }

  @SuppressLint("DefaultLocale")
  private void setData(int posicion) {

    if (posicion < pesadas.size()) {
      tvPesadaId.setText(String.format("Pesada %d de %d", posicion, pesadas.size()));
      spnGalpones.setSelection(Integer.parseInt(pesadas.get(posicion - 1).getGalpon()) - 1);
      tvJabasPesada.setText(String.valueOf(pesadas.get(posicion - 1).getJabas_por_pesada()));
      tvAvesJaba.setText(String.valueOf(pesadas.get(posicion - 1).getAves_por_jaba()));
    } else if (posicion == pesadas.size()) {
      tvPesadaId.setText(String.format("Pesada %d de %d", posicion, pesadas.size()));
      spnGalpones.setSelection(Integer.parseInt(pesadas.get(posicion - 1).getGalpon()) - 1);
      tvJabasPesada.setText(String.valueOf(pesadas.get(posicion - 1).getJabas_por_pesada()));
      tvAvesJaba.setText(String.valueOf(pesadas.get(posicion - 1).getAves_por_jaba()));
    }

    if (posicion == pesadas.size() && !aveFinalizada) {
      btnCerrarPesadaAves.setEnabled(false);
      btnGuardarPeso.setEnabled(true);
    }

    if (posicion == pesadas.size() && aveFinalizada) {
      btnCerrarPesadaAves.setEnabled(true);
      btnGuardarPeso.setEnabled(false);
    }

  }

  private void initViews(View view) {
    tvPesadaId = view.findViewById(R.id.tv_pesada_id);
    spnGalpones = view.findViewById(R.id.spn_galpones);
    tvJabasPesada = view.findViewById(R.id.tv_jabas_pesada);
    tvAvesJaba = view.findViewById(R.id.tv_aves_jaba);
    etCodigoBalanza = view.findViewById(R.id.et_codigo_balanza);
    etPesoAve = view.findViewById(R.id.et_peso_ave);
    button2 = view.findViewById(R.id.button2);
    btnGuardarPeso = view.findViewById(R.id.btn_guardar_peso);
    btnListarAvesPesada = view.findViewById(R.id.btn_listar_aves_pesadas);
    btnCerrarPesadaAves = view.findViewById(R.id.btn_cerrar_pesada_aves);
  }

  private void initUI() {
    setupButtons();
    setupSpinners();
  }

  private void setupButtons() {
    btnGuardarPeso.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        btnGuardarPeso.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

            Double avePeso = 0.0;
            if (etPesoAve.getText().length() != 0) {
              avePeso = Double.parseDouble(etPesoAve.getText().toString());
            }

            int galpon = Integer.parseInt(spnGalpones.getSelectedItem().toString());
            String codigoBalanza = etCodigoBalanza.getText().toString();

            if (!validarBalanza()) {
              showMessageError("ERROR!\nCódigo de Balanza    ");
              return;
            }
            if (validarPeso()) {
              if (posicion < pesadas.size()) {
                showMessageSuccess();
                PesajeDatabse.getInstance(getContext())
                    .pesadaStore()
                    .updatePesoAve(avePeso, codigoBalanza, galpon, pesadas.get(posicion - 1)
                        .getPesadaId());
                getData();
              }

              if (posicion == pesadas.size()) {
                showMessageSuccess();
                PesajeDatabse.getInstance(getContext())
                    .pesadaStore()
                    .updatePesoAve(avePeso, codigoBalanza, galpon, pesadas.get(posicion - 1)
                        .getPesadaId());

                aveFinalizada = true;

                getData();
              }
            } else {
              showMessageError("ERROR!\nEl peso es 0    ");
            }
          }
        });
      }
    });

    btnCerrarPesadaAves.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        PesajeDatabse.getInstance(getContext())
            .pedidoStore()
            .cerrarPesajeAvesHembra(pedidoId);
      }
    });

    btnListarAvesPesada.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(getActivity(), PesajeTarasHembraListaActivity.class).putExtra(
            "pedidoId", pedidoId).putExtra("sexo", "F"));
      }
    });
  }


  private void showMessageSuccess() {
    LayoutInflater inflater = getLayoutInflater();
    View view = inflater.inflate(R.layout.toast_success,
        (ViewGroup) getActivity().findViewById(R.id.constraintLayoutForSuccess));

    TextView tvSuccess = view.findViewById(R.id.tv_success);

    Toast toast = new Toast(getContext());
    tvSuccess.setText("Guardado!    ");
    toast.setView(view);
    toast.setDuration(Toast.LENGTH_SHORT);
    toast.show();
  }

  private void showMessageError(String message) {
    LayoutInflater inflater = getLayoutInflater();
    View view = inflater.inflate(R.layout.toast_error,
        (ViewGroup) getActivity().findViewById(R.id.constraintLayoutForError));

    TextView tvError = view.findViewById(R.id.tv_error);

    Toast toast = new Toast(getContext());
    tvError.setText(message);
    toast.setView(view);
    toast.setDuration(Toast.LENGTH_SHORT);
    toast.show();
  }

  private boolean validarPeso() {
    return etPesoAve.getText() != null && etPesoAve.getText().length() != 0 && Double.parseDouble(etPesoAve.getText().toString()) != 0.0;
  }

  private boolean validarBalanza() {
    return etCodigoBalanza.getText() != null && etCodigoBalanza.length() != 0;
  }

  private void setupSpinners() {
    List<String> galpones = setCantidadGalpones();

    ArrayAdapter<String> galponesAdapter = new ArrayAdapter<>(getActivity(),
        android.R.layout.simple_spinner_item, galpones);

    galponesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    spnGalpones.setAdapter(galponesAdapter);

  }

  private List<String> setCantidadGalpones() {
    List<String> galpones = new ArrayList<>();

    for (int i = 1; i <= 25; i++) {
      galpones.add("" + i + "");
    }

    return galpones;
  }

  private class GetPesadas extends AsyncTask<Void, Void, List<Pesada>> {

    @Override
    protected List<Pesada> doInBackground(Void... voids) {
      pesadas = PesajeDatabse.getInstance(getContext()).pesadaStore().getPesadas(pedidoId, "F");
      return pesadas;
    }

    @Override
    protected void onPostExecute(List<Pesada> pesadas) {
      Log.d("PESADA", pesadas.toString());
      calcularPosicion(pesadas);
    }
  }
}
