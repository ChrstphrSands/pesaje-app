package com.example.pesajeapp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;

import com.example.pesajeapp.model.Pedido;
import com.example.pesajeapp.model.Pesada;
import com.example.pesajeapp.model.Usuario;
import com.example.pesajeapp.stores.PedidoStore;
import com.example.pesajeapp.stores.PesadaStore;
import com.example.pesajeapp.stores.UsuarioStore;

@Database(entities = {Pedido.class, Pesada.class, Usuario.class}, exportSchema = false, version = 1)
public abstract class PesajeDatabse extends androidx.room.RoomDatabase {

  private static final String DB_NAME = "pesaje_db";
  private static PesajeDatabse INSTANCE;

  public static synchronized PesajeDatabse getInstance(Context context) {
    if (INSTANCE == null) {
      INSTANCE = Room.databaseBuilder(context.getApplicationContext(), PesajeDatabse.class,
          DB_NAME)
          .allowMainThreadQueries()
          .build();
    }
    return INSTANCE;
  }

  public static void destroyInstance() {
    INSTANCE = null;
  }

  public abstract PedidoStore pedidoStore();
  public abstract PesadaStore pesadaStore();
  public abstract UsuarioStore usuarioStore();
}
