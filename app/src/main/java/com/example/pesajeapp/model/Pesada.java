package com.example.pesajeapp.model;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;


@Entity(tableName = "pesada", foreignKeys = @ForeignKey(entity = Pedido.class, parentColumns =
    "pedidoId",
    childColumns = "pedidoId",
    onDelete = CASCADE
), indices = {@Index("pedidoId")})
public class Pesada {
  @PrimaryKey(autoGenerate = true)
  @NonNull
  private int pesadaId;
  @ColumnInfo(name = "tara_peso")
  private double tara_peso;
  @ColumnInfo(name = "tara_pesada")
  private boolean tara_pesada;
  @ColumnInfo(name = "ave_peso")
  private double ave_peso;
  @ColumnInfo(name = "ave_pesada")
  private boolean ave_pesada;
  @ColumnInfo(name = "jabas_por_pesada")
  private int jabas_por_pesada;
  @ColumnInfo(name = "aves_por_jaba")
  private int aves_por_jaba;
  @ColumnInfo(name = "codigo_balanza")
  private String codigo_balanza;
  @ColumnInfo(name = "galpon")
  private String galpon;
  @ColumnInfo(name = "sexo")
  private String sexo;
  @ColumnInfo(name = "imprimible")
  private boolean imprimible;
  @ColumnInfo(name = "finalizado")
  private boolean finalizado;
  @ColumnInfo(name = "pedidoId")
  private String pedidoId;

  public Pesada() {
  }

  public int getPesadaId() {
    return pesadaId;
  }

  public void setPesadaId(int pesadaId) {
    this.pesadaId = pesadaId;
  }

  public double getTara_peso() {
    return tara_peso;
  }

  public void setTara_peso(double tara_peso) {
    this.tara_peso = tara_peso;
  }

  public boolean isTara_pesada() {
    return tara_pesada;
  }

  public void setTara_pesada(boolean tara_pesada) {
    this.tara_pesada = tara_pesada;
  }

  public double getAve_peso() {
    return ave_peso;
  }

  public void setAve_peso(double ave_peso) {
    this.ave_peso = ave_peso;
  }

  public boolean isAve_pesada() {
    return ave_pesada;
  }

  public void setAve_pesada(boolean ave_pesada) {
    this.ave_pesada = ave_pesada;
  }

  public int getJabas_por_pesada() {
    return jabas_por_pesada;
  }

  public void setJabas_por_pesada(int jabas_por_pesada) {
    this.jabas_por_pesada = jabas_por_pesada;
  }

  public int getAves_por_jaba() {
    return aves_por_jaba;
  }

  public void setAves_por_jaba(int aves_por_jaba) {
    this.aves_por_jaba = aves_por_jaba;
  }

  public String getCodigo_balanza() {
    return codigo_balanza;
  }

  public void setCodigo_balanza(String codigo_balanza) {
    this.codigo_balanza = codigo_balanza;
  }

  public String getGalpon() {
    return galpon;
  }

  public void setGalpon(String galpon) {
    this.galpon = galpon;
  }

  public String getSexo() {
    return sexo;
  }

  public void setSexo(String sexo) {
    this.sexo = sexo;
  }

  public boolean isImprimible() {
    return imprimible;
  }

  public void setImprimible(boolean imprimible) {
    this.imprimible = imprimible;
  }

  public boolean isFinalizado() {
    return finalizado;
  }

  public void setFinalizado(boolean finalizado) {
    this.finalizado = finalizado;
  }

  public String getPedidoId() {
    return pedidoId;
  }

  public void setPedidoId(String pedidoId) {
    this.pedidoId = pedidoId;
  }

  @Override
  public String toString() {
    return "Pesada{" +
        "pesadaId=" + pesadaId +
        ", tara_peso=" + tara_peso +
        ", tara_pesada=" + tara_pesada +
        ", ave_peso=" + ave_peso +
        ", ave_pesada=" + ave_pesada +
        ", jabas_por_pesada=" + jabas_por_pesada +
        ", aves_por_jaba=" + aves_por_jaba +
        ", codigo_balanza='" + codigo_balanza + '\'' +
        ", galpon='" + galpon + '\'' +
        ", sexo='" + sexo + '\'' +
        ", imprimible=" + imprimible +
        ", finalizado=" + finalizado +
        ", pedidoId='" + pedidoId + '\'' +
        '}';
  }
}
