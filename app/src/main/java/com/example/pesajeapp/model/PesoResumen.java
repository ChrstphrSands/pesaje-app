package com.example.pesajeapp.model;

public class PesoResumen {

  private int cantidad_pesadas;
  private int jaba_pesada;
  private int aves_jaba;

  public int getCantidad_pesadas() {
    return cantidad_pesadas;
  }

  public void setCantidad_pesadas(int cantidad_pesadas) {
    this.cantidad_pesadas = cantidad_pesadas;
  }

  public int getJaba_pesada() {
    return jaba_pesada;
  }

  public void setJaba_pesada(int jaba_pesada) {
    this.jaba_pesada = jaba_pesada;
  }

  public int getAves_jaba() {
    return aves_jaba;
  }

  public void setAves_jaba(int aves_jaba) {
    this.aves_jaba = aves_jaba;
  }

  @Override
  public String toString() {
    return "PesoResumen{" +
        "cantidad_pesadas=" + cantidad_pesadas +
        ", jaba_pesada=" + jaba_pesada +
        ", aves_jaba=" + aves_jaba +
        '}';
  }
}
