package com.example.pesajeapp.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "pedido")
public class Pedido {
  @PrimaryKey
  @NonNull
  private String pedidoId;
  @ColumnInfo(name = "nombre_cliente")
  private String nombre_cliente;
  @ColumnInfo(name = "ruc_cliente")
  private String ruc_cliente;
  @ColumnInfo(name = "fecha_pedido")
  private String fecha_pedido;
  @ColumnInfo(name = "tipo_pedido")
  private String tipo_pedido;
  @ColumnInfo(name = "chofer")
  private String chofer;
  @ColumnInfo(name = "placa")
  private String placa;
  @ColumnInfo(name = "producto")
  private String producto;
  @ColumnInfo(name = "cantidad_aves")
  private int cantidad_aves;
  @ColumnInfo(name = "cantidad_hembras")
  private int cantidad_hembras;
  @ColumnInfo(name = "cantidad_machos")
  private int cantidad_machos;
  @ColumnInfo(name = "configurado_macho")
  private boolean configurado_macho;
  @ColumnInfo(name = "configurado_hembra")
  private boolean configurado_hembra;
  @ColumnInfo(name = "tara_finalizada_macho")
  private boolean tara_finalizada_macho;
  @ColumnInfo(name = "tara_finalizada_hembra")
  private boolean tara_finalizada_hembra;
  @ColumnInfo(name = "ave_finalizada_macho")
  private boolean ave_finalizada_macho;
  @ColumnInfo(name = "ave_finalizada_hembra")
  private boolean ave_finalizada_hembra;
  @ColumnInfo(name = "finalizado")
  private boolean finalizado;

  public Pedido() {
  }

  @NonNull
  public String getPedidoId() {
    return pedidoId;
  }

  public void setPedidoId(@NonNull String pedidoId) {
    this.pedidoId = pedidoId;
  }

  public String getNombre_cliente() {
    return nombre_cliente;
  }

  public void setNombre_cliente(String nombre_cliente) {
    this.nombre_cliente = nombre_cliente;
  }

  public String getRuc_cliente() {
    return ruc_cliente;
  }

  public void setRuc_cliente(String ruc_cliente) {
    this.ruc_cliente = ruc_cliente;
  }

  public String getFecha_pedido() {
    return fecha_pedido;
  }

  public void setFecha_pedido(String fecha_pedido) {
    this.fecha_pedido = fecha_pedido;
  }

  public String getTipo_pedido() {
    return tipo_pedido;
  }

  public void setTipo_pedido(String tipo_pedido) {
    this.tipo_pedido = tipo_pedido;
  }

  public String getChofer() {
    return chofer;
  }

  public void setChofer(String chofer) {
    this.chofer = chofer;
  }

  public String getPlaca() {
    return placa;
  }

  public void setPlaca(String placa) {
    this.placa = placa;
  }

  public String getProducto() {
    return producto;
  }

  public void setProducto(String producto) {
    this.producto = producto;
  }

  public int getCantidad_aves() {
    return cantidad_aves;
  }

  public void setCantidad_aves(int cantidad_aves) {
    this.cantidad_aves = cantidad_aves;
  }

  public int getCantidad_hembras() {
    return cantidad_hembras;
  }

  public void setCantidad_hembras(int cantidad_hembras) {
    this.cantidad_hembras = cantidad_hembras;
  }

  public int getCantidad_machos() {
    return cantidad_machos;
  }

  public void setCantidad_machos(int cantidad_machos) {
    this.cantidad_machos = cantidad_machos;
  }

  public boolean isConfigurado_macho() {
    return configurado_macho;
  }

  public void setConfigurado_macho(boolean configurado_macho) {
    this.configurado_macho = configurado_macho;
  }

  public boolean isConfigurado_hembra() {
    return configurado_hembra;
  }

  public void setConfigurado_hembra(boolean configurado_hembra) {
    this.configurado_hembra = configurado_hembra;
  }

  public boolean isTara_finalizada_macho() {
    return tara_finalizada_macho;
  }

  public void setTara_finalizada_macho(boolean tara_finalizada_macho) {
    this.tara_finalizada_macho = tara_finalizada_macho;
  }

  public boolean isTara_finalizada_hembra() {
    return tara_finalizada_hembra;
  }

  public void setTara_finalizada_hembra(boolean tara_finalizada_hembra) {
    this.tara_finalizada_hembra = tara_finalizada_hembra;
  }

  public boolean isAve_finalizada_macho() {
    return ave_finalizada_macho;
  }

  public void setAve_finalizada_macho(boolean ave_finalizada_macho) {
    this.ave_finalizada_macho = ave_finalizada_macho;
  }

  public boolean isAve_finalizada_hembra() {
    return ave_finalizada_hembra;
  }

  public void setAve_finalizada_hembra(boolean ave_finalizada_hembra) {
    this.ave_finalizada_hembra = ave_finalizada_hembra;
  }

  public boolean isFinalizado() {
    return finalizado;
  }

  public void setFinalizado(boolean finalizado) {
    this.finalizado = finalizado;
  }

  @Override
  public String toString() {
    return "Pedido{" +
        "pedidoId='" + pedidoId + '\'' +
        ", nombre_cliente='" + nombre_cliente + '\'' +
        ", ruc_cliente='" + ruc_cliente + '\'' +
        ", fecha_pedido='" + fecha_pedido + '\'' +
        ", tipo_pedido='" + tipo_pedido + '\'' +
        ", chofer='" + chofer + '\'' +
        ", placa='" + placa + '\'' +
        ", producto='" + producto + '\'' +
        ", cantidad_aves=" + cantidad_aves +
        ", cantidad_hembras=" + cantidad_hembras +
        ", cantidad_machos=" + cantidad_machos +
        ", configurado_macho=" + configurado_macho +
        ", configurado_hembra=" + configurado_hembra +
        ", tara_finalizada_macho=" + tara_finalizada_macho +
        ", tara_finalizada_hembra=" + tara_finalizada_hembra +
        ", ave_finalizada_macho=" + ave_finalizada_macho +
        ", ave_finalizada_hembra=" + ave_finalizada_hembra +
        ", finalizado=" + finalizado +
        '}';
  }
}
