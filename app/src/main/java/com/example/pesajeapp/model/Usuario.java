package com.example.pesajeapp.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "usuario")
public class Usuario {
  @PrimaryKey
  @NonNull
  private int id;
  @ColumnInfo(name = "usuario")
  private String usuario;
  @ColumnInfo(name = "password")
  private String password;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUsuario() {
    return usuario;
  }

  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    return "Usuario{" +
        "id=" + id +
        ", usuario='" + usuario + '\'' +
        ", password='" + password + '\'' +
        '}';
  }
}
