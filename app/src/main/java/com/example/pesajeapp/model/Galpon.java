package com.example.pesajeapp.model;

public class Galpon {
  private String galpon;

  public String getGalpon() {
    return galpon;
  }

  public void setGalpon(String galpon) {
    this.galpon = galpon;
  }

  @Override
  public String toString() {
    return galpon;
  }
}
