package com.example.pesajeapp.model;

public class Resumen {

  private int cantidad_aves;
  private int cantidad_jabas;
  private double total_peso_taras;
  private double peso_bruto;
  private double peso_neto;
  private double peso_promedio;
  private String galpon;
  private String codigo_balanza;

  public int getCantidad_aves() {
    return cantidad_aves;
  }

  public void setCantidad_aves(int cantidad_aves) {
    this.cantidad_aves = cantidad_aves;
  }

  public int getCantidad_jabas() {
    return cantidad_jabas;
  }

  public void setCantidad_jabas(int cantidad_jabas) {
    this.cantidad_jabas = cantidad_jabas;
  }

  public double getTotal_peso_taras() {
    return total_peso_taras;
  }

  public void setTotal_peso_taras(double total_peso_taras) {
    this.total_peso_taras = total_peso_taras;
  }

  public double getPeso_bruto() {
    return peso_bruto;
  }

  public void setPeso_bruto(double peso_bruto) {
    this.peso_bruto = peso_bruto;
  }

  public double getPeso_neto() {
    return peso_neto;
  }

  public void setPeso_neto(double peso_neto) {
    this.peso_neto = peso_neto;
  }

  public double getPeso_promedio() {
    return peso_promedio;
  }

  public void setPeso_promedio(double peso_promedio) {
    this.peso_promedio = peso_promedio;
  }

  public String getGalpon() {
    return galpon;
  }

  public void setGalpon(String galpon) {
    this.galpon = galpon;
  }

  public String getCodigo_balanza() {
    return codigo_balanza;
  }

  public void setCodigo_balanza(String codigo_balanza) {
    this.codigo_balanza = codigo_balanza;
  }

  @Override
  public String toString() {
    return "Resumen{" +
        "cantidad_aves=" + cantidad_aves +
        ", cantidad_jabas=" + cantidad_jabas +
        ", total_peso_taras=" + total_peso_taras +
        ", peso_bruto=" + peso_bruto +
        ", peso_neto=" + peso_neto +
        ", peso_promedio=" + peso_promedio +
        ", galpon='" + galpon + '\'' +
        ", codigo_balanza='" + codigo_balanza + '\'' +
        '}';
  }
}
