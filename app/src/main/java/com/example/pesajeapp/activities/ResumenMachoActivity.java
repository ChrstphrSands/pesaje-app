package com.example.pesajeapp.activities;

    import android.content.Intent;
    import android.os.AsyncTask;
    import android.os.Build;
    import android.os.Bundle;
    import android.util.Log;

    import androidx.appcompat.app.AppCompatActivity;
    import androidx.appcompat.widget.Toolbar;
    import androidx.viewpager.widget.ViewPager;

    import com.example.pesajeapp.R;
    import com.example.pesajeapp.database.PesajeDatabse;
    import com.example.pesajeapp.fragments.ResumenBrutosMachoFragment;
    import com.example.pesajeapp.fragments.ResumenMachoFragment;
    import com.example.pesajeapp.fragments.ResumenTarasMachoFragment;
    import com.example.pesajeapp.adapters.ViewPagerAdapter;
    import com.example.pesajeapp.model.Pesada;
    import com.example.pesajeapp.model.PesoResumen;
    import com.google.android.material.tabs.TabLayout;

    import java.util.List;

public class ResumenMachoActivity extends AppCompatActivity {

  Toolbar toolbar;
  TabLayout tlResumen;
  ViewPager vpResumen;
  // TableLayout tblResumen;
  ViewPagerAdapter viewPagerAdapter;

  String pedidoId;
  String sexo;

  PesoResumen pesoResumen;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_resumen_macho);

    Intent intent = getIntent();
    pedidoId = intent.getStringExtra("pedidoId");
    sexo = intent.getStringExtra("sexo");

    initComponents();

    getData();

    initUI();
  }

  private void initComponents() {
    toolbar = findViewById(R.id.toolbar);
    tlResumen = findViewById(R.id.tl_resumen);
    vpResumen = findViewById(R.id.vp_pesaje_ave);
  }

  private void getData() {
    GetPesoResumen getPesoResumen = new GetPesoResumen();
    getPesoResumen.execute();
  }

  private void initUI() {
    setupToolbar();
    setupTabLayout();
    setupViewPager();
    // setupTableLayout();
  }

  private void setupToolbar() {
    Toolbar toolbar = findViewById(R.id.toolbar);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      toolbar.setNavigationIcon(R.drawable.ic_menu_24);
      toolbar.setTitle("Pedido");
    }

    try {
      toolbar.setTitleTextColor(getResources().getColor(R.color.md_white_1000));
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "No se puede cambiar el color del titulo.");
    }

    try {
      setSupportActionBar(toolbar);
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar el action bar");
    }

    try {
      if (getSupportActionBar() != null) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      }
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar la pantalla de inicio como activada");
    }
  }

  private void setupTabLayout() {

    tlResumen.setupWithViewPager(vpResumen);
    tlResumen.setInlineLabel(true);

    tlResumen.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        getSupportActionBar().setTitle(viewPagerAdapter.getPageTitle(tab.getPosition()));
        // tab.getIcon().setColorFilter(getResources().getColor(R.color.md_white_1000),
        //     PorterDuff.Mode.SRC_IN);
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {
        // tab.getIcon().setColorFilter(getResources().getColor(R.color.md_grey_500),
        //     PorterDuff.Mode.SRC_IN);
      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {

      }
    });
  }

  private void setupViewPager() {
    viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

    Bundle bundleMachos = new Bundle();
    bundleMachos.putString("pedidoId", pedidoId);
    bundleMachos.putString("sexo", "M");
    bundleMachos.putInt("cantidad_pesadas", pesoResumen.getCantidad_pesadas());
    bundleMachos.putInt("jaba_pesada", pesoResumen.getJaba_pesada());
    bundleMachos.putInt("aves_jaba", pesoResumen.getAves_jaba());

    ResumenTarasMachoFragment resumenTarasMachoFragment = new ResumenTarasMachoFragment();
    resumenTarasMachoFragment.setArguments(bundleMachos);

    ResumenBrutosMachoFragment resumenBrutosMachoFragment = new ResumenBrutosMachoFragment();
    resumenBrutosMachoFragment.setArguments(bundleMachos);

    ResumenMachoFragment resumenMachoFragment = new ResumenMachoFragment();
    resumenMachoFragment.setArguments(bundleMachos);

    viewPagerAdapter.agregarFragment(resumenTarasMachoFragment, "TARAS");
    viewPagerAdapter.agregarFragment(resumenBrutosMachoFragment, "BRUTOS");
    viewPagerAdapter.agregarFragment(resumenMachoFragment, "RESUMEN");

    vpResumen.setAdapter(viewPagerAdapter);
  }

  private class GetPesoResumen extends AsyncTask<Void, Void, PesoResumen> {

    @Override
    protected PesoResumen doInBackground(Void... voids) {
      pesoResumen = PesajeDatabse.getInstance(getApplication()).pedidoStore().getPesoResumen(pedidoId,
          sexo);
      return pesoResumen;
    }

    @Override
    protected void onPostExecute(PesoResumen pesoResumen) {
      Log.d("PESORESUMEN", pesoResumen.toString());
    }
  }

}
