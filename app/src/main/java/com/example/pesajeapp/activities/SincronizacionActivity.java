package com.example.pesajeapp.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.adapters.ResumenesAdapter;
import com.example.pesajeapp.adapters.SincronizacionAdapter;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pedido;
import com.example.pesajeapp.model.ResumenData;
import com.example.pesajeapp.model.Sincronizacion;

import java.util.ArrayList;
import java.util.List;

public class SincronizacionActivity extends AppCompatActivity {

  private Toolbar toolbar;
  private Button btnSincronizarAtrapados;
  private RecyclerView rvResumen;

  private ResumenesAdapter resumenesAdapter;
  private SincronizacionAdapter sincronizacionAdapter;

  private List<ResumenData> resumenDataList;
  private List<Sincronizacion> sincronizacionList;

  private int cantidadPedidos;
  private int cantidadRealizados;
  private int cantidadCerrados;
  private List<Integer> cantidades;
  private List<String> descripciones;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sincronizacion);
    initData();
    initViews();
    initUI();
    initActions();
  }

  private void initViews() {
    toolbar = findViewById(R.id.toolbar);
    rvResumen = findViewById(R.id.rv_resumen);
    btnSincronizarAtrapados = findViewById(R.id.btn_sincronizar_atrapados);
  }

  private void initUI() {
    initToolbar();
    initRecyclerViewResumen();
  }

  private void initData() {
    getCantidadPedidos();
    getDescripcionPedidos();
    setResumenes();
  }

  private void setResumenes() {
    resumenDataList = new ArrayList<>();

    for (int i = 0; i < cantidades.size(); i++) {
      ResumenData resumenData = new ResumenData();
      resumenData.setCantidad(cantidades.get(i));
      resumenData.setDescripcion(descripciones.get(i));
      resumenDataList.add(resumenData);
    }
  }

  private void initActions() {
    btnSincronizarAtrapados.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        initDataPedidos();
      }
    });
  }

  private void initToolbar() {
    Toolbar toolbar = findViewById(R.id.toolbar);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      toolbar.setNavigationIcon(R.drawable.ic_menu_24);
      toolbar.setTitle("Dashboard");
    }

    try {
      toolbar.setTitleTextColor(getResources().getColor(R.color.md_white_1000));
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "No se puede cambiar el color del titulo.");
    }

    try {
      setSupportActionBar(toolbar);
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar el action bar");
    }

    try {
      if (getSupportActionBar() != null) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      }
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar la pantalla de inicio como activada");
    }
  }

  private void getCantidadPedidos() {
    cantidades = new ArrayList<>();
    cantidadPedidos = PesajeDatabse.getInstance(getApplicationContext())
        .pedidoStore()
        .getTotalPedidos();
    cantidadRealizados = PesajeDatabse.getInstance(getApplicationContext())
        .pedidoStore()
        .getRealizados();
    cantidadCerrados = PesajeDatabse.getInstance(getApplicationContext())
        .pedidoStore()
        .getCerrados();

    cantidades.add(cantidadPedidos);
    cantidades.add(cantidadRealizados);
    cantidades.add(cantidadCerrados);
  }

  private void getDescripcionPedidos() {
    descripciones = new ArrayList<>();

    descripciones.add("Ordenes de Ingreso");
    descripciones.add("Atrapados Realizados");
    descripciones.add("Atrapados Cerrados");
  }

  private void initRecyclerViewResumen() {
    resumenesAdapter = new ResumenesAdapter(resumenDataList, new ResumenesAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(ResumenData resumenData) {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
      }
    });

    RecyclerView.LayoutManager resumenLayoutManager =
        new GridLayoutManager(getApplicationContext(), cantidades.size());
    rvResumen.setLayoutManager(resumenLayoutManager);
    rvResumen.setAdapter(resumenesAdapter);
  }

  private void initDataPedidos() {
    Pedido pedido = new Pedido();

    for (int i = 1; i <= 10; i++) {

      pedido.setPedidoId("" + i);
      pedido.setNombre_cliente("Carlos " + i);
      pedido.setRuc_cliente("Ruc " + i * 1111111);
      pedido.setTipo_pedido("Granja");
      pedido.setFecha_pedido("19/08/2019");
      pedido.setChofer("Mamani " + i);
      pedido.setPlaca("AZO" + i * 111);
      pedido.setProducto("123123");
      pedido.setCantidad_aves(4000);
      pedido.setCantidad_machos(1300);
      pedido.setCantidad_hembras(2700);
      pedido.setConfigurado_hembra(false);
      pedido.setConfigurado_macho(false);
      pedido.setFinalizado(false);
      PesajeDatabse.getInstance(getApplicationContext()).pedidoStore().insertPedido(pedido);
    }

  }
}
