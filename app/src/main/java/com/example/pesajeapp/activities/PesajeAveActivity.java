package com.example.pesajeapp.activities;

import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.example.pesajeapp.R;
import com.example.pesajeapp.adapters.ViewPagerAdapter;
import com.example.pesajeapp.fragments.EdicionPolloHembraFragment;
import com.example.pesajeapp.fragments.EdicionPolloMachoFragment;
import com.example.pesajeapp.fragments.PesajeAveHembraFragment;
import com.example.pesajeapp.fragments.PesajeAveMachoFragment;
import com.google.android.material.tabs.TabLayout;

public class PesajeAveActivity extends AppCompatActivity {

  TabLayout tlPesajeAve;
  ViewPager vpPesajeAve;
  Toolbar toolbar;
  ViewPagerAdapter viewPagerAdapter;
  String pedidoId;
  boolean aveFinalizada;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pesaje_ave);

    pedidoId = getIntent().getStringExtra("pedidoId");
    aveFinalizada = getIntent().getBooleanExtra("ave_finalizada", false);

    initViews();
    initUI();
  }

  private void initViews() {
    toolbar = findViewById(R.id.toolbar);
    tlPesajeAve = findViewById(R.id.tl_pesaje_ave);
    vpPesajeAve = findViewById(R.id.vp_pesaje_ave);
  }

  private void initUI() {
    setupToolbar();
    setupViewPager();
    setupTabLayout();
  }

  private void setupToolbar() {

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      toolbar.setNavigationIcon(R.drawable.ic_menu_24);
      toolbar.setTitle("Pedido");
    }

    try {
      toolbar.setTitleTextColor(getResources().getColor(R.color.md_white_1000));
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "No se puede cambiar el color del titulo.");
    }

    try {
      setSupportActionBar(toolbar);
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar el action bar");
    }

    try {
      if (getSupportActionBar() != null) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      }
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar la pantalla de inicio como activada");
    }
  }

  private void setupTabLayout() {

    tlPesajeAve.setupWithViewPager(vpPesajeAve);
    tlPesajeAve.setInlineLabel(true);

    tlPesajeAve.getTabAt(0).setIcon(R.drawable.male_24px);
    tlPesajeAve.getTabAt(1).setIcon(R.drawable.female_24px);

    tlPesajeAve.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.md_white_1000)
        , PorterDuff.Mode.SRC_IN);
    tlPesajeAve.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.md_white_1000)
        , PorterDuff.Mode.SRC_IN);

    tlPesajeAve.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        getSupportActionBar().setTitle(viewPagerAdapter.getPageTitle(tab.getPosition()));
        // tab.getIcon().setColorFilter(getResources().getColor(R.color.md_white_1000),
        //     PorterDuff.Mode.SRC_IN);
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {
        // tab.getIcon().setColorFilter(getResources().getColor(R.color.md_grey_500),
        //     PorterDuff.Mode.SRC_IN);
      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {

      }
    });
  }

  private void setupViewPager() {
    viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

    Bundle bundle = new Bundle();
    bundle.putString("pedidoId", pedidoId);
    bundle.putBoolean("ave_finalizada", aveFinalizada);

    PesajeAveMachoFragment pesajeAveMachoFragment = new PesajeAveMachoFragment();
    pesajeAveMachoFragment.setArguments(bundle);

    PesajeAveHembraFragment pesajeAveHembraFragment = new PesajeAveHembraFragment();
    pesajeAveHembraFragment.setArguments(bundle);

    viewPagerAdapter.agregarFragment(pesajeAveMachoFragment, "PESAJE AVE MACHO");
    viewPagerAdapter.agregarFragment(pesajeAveHembraFragment, "PESAJE AVE HEMBRA");

    vpPesajeAve.setAdapter(viewPagerAdapter);
  }


}
