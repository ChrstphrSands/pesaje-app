package com.example.pesajeapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.example.pesajeapp.adapters.PedidosAdapter;
import com.example.pesajeapp.R;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pedido;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private RecyclerView recyclerView;
  private PedidosAdapter pedidosAdapter;
  // private InsertPedidos insertPedidos;
  private InsertPedido insertPedido;
  private SelectPedidos selectPedidos;
  private List<Pedido> pedidos;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    initUI();


  }

  private void initUI() {
    initToolbar();
    // initRecyclerView();

    // initDataPedidos();
    selectPedidos = new SelectPedidos();
    selectPedidos.execute();
  }

  private void initToolbar() {
    Toolbar toolbar = findViewById(R.id.toolbar);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      toolbar.setNavigationIcon(R.drawable.ic_menu_24);
      toolbar.setTitle("Listado de pedidos");
    }

    try {
      toolbar.setTitleTextColor(getResources().getColor(R.color.md_white_1000));
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "No se puede cambiar el color del titulo.");
    }

    try {
      setSupportActionBar(toolbar);
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar el action bar");
    }

    try {
      if (getSupportActionBar() != null) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      }
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar la pantalla de inicio como activada");
    }
  }

  private void initRecyclerView(List<Pedido> pedidos) {
    recyclerView = findViewById(R.id.rv_pedido);

    recyclerView.setLayoutManager(new LinearLayoutManager(this));

    pedidosAdapter = new PedidosAdapter(pedidos, new PedidosAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(Pedido pedido) {

        Intent intent = new Intent(getApplicationContext(), EdicionPedidoActivity.class);
        intent.putExtra("pedidoId", pedido.getPedidoId());
        startActivity(intent);
      }
    });

    recyclerView.setAdapter(pedidosAdapter);
  }



  private class InsertPedido extends AsyncTask<Pedido, Void, Void> {

    @Override
    protected Void doInBackground(Pedido... pedidos) {
      PesajeDatabse.getInstance(getApplicationContext()).pedidoStore().insertPedido(pedidos[0]);

      return null;
    }
  }


  // private class InsertPedidos extends AsyncTask<List<Pedido>, Void, Void> {
  //
  //   @Override
  //   protected Void doInBackground(List<Pedido>... lists) {
  //     PesajeDatabse.getInstance(getApplicationContext()).pedidoStore().insertPedido(lists);
  //     return null;
  //   }
  // }

  private class SelectPedidos extends AsyncTask<Void, Void, List<Pedido>> {

    @Override
    protected List<Pedido> doInBackground(Void... voids) {
      pedidos = PesajeDatabse.getInstance(getApplication()).pedidoStore().getPedidos();
      return pedidos;
    }

    @Override
    protected void onPostExecute(List<Pedido> pedidos) {
      initRecyclerView(pedidos);
      showPedidos(pedidos);
    }
  }

  private void showPedidos(List<Pedido> pedidos) {
    for (int i = 0; i < pedidos.size(); i++) {
      Log.d("Pedidos", pedidos.get(i).toString());
    }
  }

}
