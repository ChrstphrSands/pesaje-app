package com.example.pesajeapp.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.pesajeapp.R;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pesada;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class PesajeTaraActivity extends AppCompatActivity {

  Toolbar toolbar;
  int pesadaId;
  List<Pesada> pesadas;
  TextView tvPesadaId;
  Spinner spnGalponesMacho;
  TextView tvJabasPesadaMacho;
  TextView tvAvesJabaMacho;
  EditText etCodigoBalanzaMacho;
  Button btnGuardarPeso;
  EditText etPesoTara;
  Button btnCerrarPesadaTara;
  Boolean taraFinalizada;

  String pedidoId;
  String sexo;
  int posicion;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pesaje);
    pesadaId = getIntent().getIntExtra("pesadaId", 0);
    pedidoId = getIntent().getStringExtra("pedidoId");
    sexo = getIntent().getStringExtra("sexo");
    taraFinalizada = getIntent().getBooleanExtra("tara_finalizada", false);
    initComponents();
    getDataPesada();
    initUI();
  }

  private void getDataPesada() {
    GetPesadas getPesadas = new GetPesadas();
    getPesadas.execute();
  }

  private void calcularPosicion(List<Pesada> pesadas) {
    posicion = pesadas.size();
    for (int i = 1; i <= pesadas.size(); i++) {
      if (!pesadas.get(i - 1).isTara_pesada()) {
        posicion = i;
        break;
      }
    }
    setData(posicion);
  }


  @SuppressLint("DefaultLocale")
  private void setData(int posicion) {

    if (posicion < pesadas.size()) {
      tvPesadaId.setText(String.format("Pesada %d de %d", posicion, pesadas.size()));
      spnGalponesMacho.setSelection(Integer.parseInt(pesadas.get(posicion - 1).getGalpon()) - 1);
      tvJabasPesadaMacho.setText(String.valueOf(pesadas.get(posicion - 1).getJabas_por_pesada()));
      tvAvesJabaMacho.setText(String.valueOf(pesadas.get(posicion - 1).getAves_por_jaba()));
    } else if (posicion == pesadas.size()) {
      tvPesadaId.setText(String.format("Pesada %d de %d", posicion, pesadas.size()));
      spnGalponesMacho.setSelection(Integer.parseInt(pesadas.get(posicion - 1).getGalpon()) - 1);
      tvJabasPesadaMacho.setText(String.valueOf(pesadas.get(posicion - 1).getJabas_por_pesada()));
      tvAvesJabaMacho.setText(String.valueOf(pesadas.get(posicion - 1).getAves_por_jaba()));
    }

    if (posicion == pesadas.size() && !taraFinalizada) {
      btnCerrarPesadaTara.setEnabled(false);
      btnGuardarPeso.setEnabled(true);
    }

    if (posicion == pesadas.size() && taraFinalizada) {
      btnGuardarPeso.setEnabled(false);
      btnCerrarPesadaTara.setEnabled(true);
    }
  }

  private void initComponents() {
    toolbar = findViewById(R.id.toolbar);
    tvPesadaId = findViewById(R.id.tv_pesada_id);
    spnGalponesMacho = findViewById(R.id.spn_galpones);
    tvJabasPesadaMacho = findViewById(R.id.tv_jabas_pesada);
    tvAvesJabaMacho = findViewById(R.id.tv_aves_jaba);
    etCodigoBalanzaMacho = findViewById(R.id.et_codigo_balanza);
    btnGuardarPeso = findViewById(R.id.btn_guardar_peso);
    etPesoTara = findViewById(R.id.et_peso_ave);
    btnCerrarPesadaTara = findViewById(R.id.btn_cerrar_pesada_tara);
  }

  private void initUI() {
    setupToolbar();
    setupSpinners();
    setupButtons();
  }

  private void setupToolbar() {
    Toolbar toolbar = findViewById(R.id.toolbar);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      toolbar.setNavigationIcon(R.drawable.ic_menu_24);
      toolbar.setTitle("Pesaje Tara");
    }

    try {
      toolbar.setTitleTextColor(getResources().getColor(R.color.md_white_1000));
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "No se puede cambiar el color del titulo.");
    }

    try {
      setSupportActionBar(toolbar);
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar el action bar");
    }

    try {
      if (getSupportActionBar() != null) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      }
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar la pantalla de inicio como activada");
    }
  }

  private void setupSpinners() {
    List<String> galpones = setCantidadGalpones();

    ArrayAdapter<String> galponesAdapter = new ArrayAdapter<>(getApplicationContext(),
        android.R.layout.simple_spinner_item, galpones);

    galponesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    spnGalponesMacho.setAdapter(galponesAdapter);

  }

  private void setupButtons() {
    btnGuardarPeso.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Double taraPeso = 0.0;
        if (etPesoTara.getText().length() != 0) {
           taraPeso = Double.parseDouble(etPesoTara.getText().toString());
        }

        int galpon = Integer.parseInt(spnGalponesMacho.getSelectedItem().toString());
        String codigoBalanza = etCodigoBalanzaMacho.getText().toString();

        if (!validarBalanza()) {
          showMessageError("ERROR!\nCódigo de Balanza    ");
          return;
        }

        if (validarPeso()) {
          if (posicion < pesadas.size()) {

            showMessageSuccess();
            PesajeDatabse.getInstance(getApplicationContext())
                .pesadaStore()
                .updatePesoTara(taraPeso, codigoBalanza, galpon, pesadas.get(posicion - 1)
                    .getPesadaId());

            getDataPesada();
          }

          if (posicion == pesadas.size()) {
            showMessageSuccess();

            PesajeDatabse.getInstance(getApplicationContext())
                .pesadaStore()
                .updatePesoTara(taraPeso, codigoBalanza, galpon, pesadas.get(posicion - 1)
                    .getPesadaId());

            taraFinalizada = true;

            getDataPesada();

          }

        } else {
          showMessageError("ERROR!\nEl peso es 0    ");
        }

      }
    });

    btnCerrarPesadaTara.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if (sexo.equals("M")) {
          PesajeDatabse.getInstance(getApplicationContext()).pedidoStore().cerrarPesajeTarasMacho(pedidoId);
          reiniciarActivity();
        } else if (sexo.equals("F")) {
          PesajeDatabse.getInstance(getApplicationContext()).pedidoStore().cerrarPesajeTarasHembra(pedidoId);
          reiniciarActivity();
        }

        for (Pesada pesada : pesadas) {
          Log.i("CERRAR", pesada.toString());
        }
      }
    });
  }

  private void showMessageSuccess() {
    LayoutInflater inflater = getLayoutInflater();
    View view = inflater.inflate(R.layout.toast_success,
        (ViewGroup) findViewById(R.id.constraintLayoutForSuccess));

    TextView tvSuccess = view.findViewById(R.id.tv_success);

    Toast toast = new Toast(this);
    tvSuccess.setText("Guardado!    ");
    toast.setView(view);
    toast.setDuration(Toast.LENGTH_SHORT);
    toast.show();
  }

  private void showMessageError(String message) {
    LayoutInflater inflater = getLayoutInflater();
    View view = inflater.inflate(R.layout.toast_error,
        (ViewGroup) findViewById(R.id.constraintLayoutForError));

    TextView tvError = view.findViewById(R.id.tv_error);

    Toast toast = new Toast(this);
    tvError.setText(message);
    toast.setView(view);
    toast.setDuration(Toast.LENGTH_SHORT);
    toast.show();
  }

  private boolean validarPeso() {
    return etPesoTara.getText() != null && etPesoTara.getText().length() != 0 && Double.parseDouble(etPesoTara.getText().toString()) != 0.0;
  }

  private boolean validarBalanza() {
    return etCodigoBalanzaMacho.getText() != null && etCodigoBalanzaMacho.length() != 0;
  }

  private void reiniciarActivity() {
    Intent intent=new Intent(this, EdicionPedidoActivity.class);
    intent.putExtra("pedidoId", pedidoId);
    startActivity(intent);
    finish();
  }

  private List<String> setCantidadGalpones() {
    List<String> galpones = new ArrayList<>();

    for (int i = 1; i <= 25; i++) {
      galpones.add("" + i + "");
    }

    return galpones;
  }

  private class GetPesadas extends AsyncTask<Void, Void, List<Pesada>> {

    @Override
    protected List<Pesada> doInBackground(Void... voids) {
      pesadas = PesajeDatabse.getInstance(getApplication()).pesadaStore().getPesadas(pedidoId,
          sexo);
      return pesadas;
    }

    @Override
    protected void onPostExecute(List<Pesada> pesadas) {
      calcularPosicion(pesadas);
    }
  }
}
