package com.example.pesajeapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pesajeapp.R;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pedido;
import com.example.pesajeapp.model.Usuario;

public class LoginActivity extends AppCompatActivity {

  private Button btnIngresar;
  private EditText etUsuario;
  private EditText etPassword;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    // initDataUsuarios();
    initViews();
    setupButtons();
  }

  private void initViews() {
    btnIngresar = findViewById(R.id.btn_ingresar);
    etUsuario = findViewById(R.id.et_usuario);
    etPassword = findViewById(R.id.et_password);
  }

  private void setupButtons() {
    btnIngresar.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        login();
      }
    });
  }

  private void login() {
    String usuario = etUsuario.getText().toString();
    String password = etPassword.getText().toString();

    if (validateData(usuario, password)) {
      // Usuario usuarioq = PesajeDatabse.getInstance(getApplicationContext())
      //     .usuarioStore()
      //     .login(usuario, password);

      if (usuario.equals("usuario") && password.equals("password")) {
        startActivity(new Intent(getApplicationContext(), SincronizacionActivity.class));
        finish();
      } else {
        Toast.makeText(this, "ERROR", Toast.LENGTH_SHORT).show();
      }
    }
  }

  private boolean validateData(String usuario, String password) {
    return true;
  }

  private void initDataUsuarios() {
    Usuario usuario = new Usuario();

    for (int i = 1; i <= 10; i++) {
      usuario.setId(i);
      usuario.setUsuario("usuario" + i);
      usuario.setPassword("password" + i);
      PesajeDatabse.getInstance(getApplicationContext())
          .usuarioStore()
          .insertUsuario(usuario);
    }
  }
}
