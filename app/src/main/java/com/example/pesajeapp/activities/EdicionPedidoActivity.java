package com.example.pesajeapp.activities;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.example.pesajeapp.R;
import com.example.pesajeapp.adapters.ViewPagerAdapter;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.fragments.EdicionPolloMachoFragment;
import com.example.pesajeapp.fragments.EdicionPolloHembraFragment;
import com.example.pesajeapp.model.Pedido;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class EdicionPedidoActivity extends AppCompatActivity {


  Spinner spnGalponesMacho;
  Spinner spnJabasXPesadaMacho;
  Spinner spnAvesXJabaMacho;
  Spinner spnGalponesHembra;
  Spinner spnJabasXPesadaHembra;
  Spinner spnAvesXJabaHembra;

  Toolbar toolbar;
  TabLayout tabLayout;
  ViewPager viewPager;
  ViewPagerAdapter viewPagerAdapter;
  TextView tvPedidoNroDato;
  TextView tvNombreCliente;
  TextView tvChofer;
  TextView tvPlaca;
  TextView tvCodigoProducto;

  Button btnHoraInicio;
  Button btnHoraFinal;

  Pedido pedido;

  Calendar dateTime = Calendar.getInstance();

  TimePickerDialog.OnTimeSetListener horaInicio = new TimePickerDialog.OnTimeSetListener() {
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
      dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
      dateTime.set(Calendar.MINUTE, minute);
      EdicionPedidoActivity.this.updateHoraInicio();
    }
  };

  TimePickerDialog.OnTimeSetListener horaFin = new TimePickerDialog.OnTimeSetListener() {
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
      dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
      dateTime.set(Calendar.MINUTE, minute);
      EdicionPedidoActivity.this.updateHoraFin();
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edicion_pedido);

    Intent intent = getIntent();
    String pedidoId = intent.getStringExtra("pedidoId");

    initComponents();
    initData(pedidoId);
    initUI();
    initActions();
  }

  private void initData(String pedidoId) {
    pedido = PesajeDatabse.getInstance(this).pedidoStore().getPedido(pedidoId);
    setData(pedido);
  }

  private void setData(Pedido pedido) {
    tvPedidoNroDato.setText(pedido.getPedidoId());
    tvNombreCliente.setText(pedido.getNombre_cliente());
    tvChofer.setText(pedido.getChofer());
    tvPlaca.setText(pedido.getPlaca());
    tvCodigoProducto.setText(pedido.getProducto());
  }

  private void initComponents() {
    toolbar = findViewById(R.id.toolbar);

    spnGalponesMacho = findViewById(R.id.spn_galpones);
    spnJabasXPesadaMacho = findViewById(R.id.tv_jabas_pesada);
    spnAvesXJabaMacho = findViewById(R.id.tv_aves_jaba);
    spnGalponesHembra = findViewById(R.id.spn_galpones_hembra);
    spnJabasXPesadaHembra = findViewById(R.id.spn_jabas_pesada_hembra);
    spnAvesXJabaHembra = findViewById(R.id.spn_aves_jaba_hembra);

    tvPedidoNroDato = findViewById(R.id.tv_pedido_nro_dato);
    tvNombreCliente = findViewById(R.id.tv_nombre_cliente);
    tvChofer = findViewById(R.id.tv_chofer);
    tvPlaca = findViewById(R.id.tv_placa);
    tvCodigoProducto = findViewById(R.id.tv_codigo_producto);

    tabLayout = findViewById(R.id.tl_resumen);

    viewPager = findViewById(R.id.vp_pesaje_ave);

    btnHoraInicio = findViewById(R.id.btn_hora_inicio);
    btnHoraFinal = findViewById(R.id.btn_hora_fin);
  }

  private void initUI() {
    setupToolbar();
    setupViewPager();
    setupTabLayout();
  }

  private void setupToolbar() {
    Toolbar toolbar = findViewById(R.id.toolbar);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      toolbar.setNavigationIcon(R.drawable.ic_menu_24);
      toolbar.setTitle("Pedido");
    }

    try {
      toolbar.setTitleTextColor(getResources().getColor(R.color.md_white_1000));
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "No se puede cambiar el color del titulo.");
    }

    try {
      setSupportActionBar(toolbar);
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar el action bar");
    }

    try {
      if (getSupportActionBar() != null) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      }
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar la pantalla de inicio como activada");
    }
  }

  private void setupTabLayout() {

    tabLayout.setupWithViewPager(viewPager);
    tabLayout.setInlineLabel(true);

    tabLayout.getTabAt(0).setIcon(R.drawable.male_24px);
    tabLayout.getTabAt(1).setIcon(R.drawable.female_24px);

    tabLayout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.md_white_1000)
        , PorterDuff.Mode.SRC_IN);
    tabLayout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.md_white_1000)
        , PorterDuff.Mode.SRC_IN);

    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        getSupportActionBar().setTitle(viewPagerAdapter.getPageTitle(tab.getPosition()));
        // tab.getIcon().setColorFilter(getResources().getColor(R.color.md_white_1000),
        //     PorterDuff.Mode.SRC_IN);
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {
        // tab.getIcon().setColorFilter(getResources().getColor(R.color.md_grey_500),
        //     PorterDuff.Mode.SRC_IN);
      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {

      }
    });
  }

  private void setupViewPager() {
    viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

    Bundle bundleMachos = new Bundle();
    bundleMachos.putInt("cantidad_machos", pedido.getCantidad_machos());
    bundleMachos.putBoolean("finalizado", pedido.isFinalizado());
    bundleMachos.putBoolean("configurado_macho", pedido.isConfigurado_macho());
    bundleMachos.putString("pedidoId", pedido.getPedidoId());
    bundleMachos.putBoolean("tara_finalizada_macho", pedido.isTara_finalizada_macho());
    bundleMachos.putBoolean("ave_finalizada", pedido.isAve_finalizada_macho());

    Bundle bundleHembras = new Bundle();
    bundleHembras.putInt("cantidad_hembras", pedido.getCantidad_hembras());
    bundleHembras.putBoolean("finalizado", pedido.isFinalizado());
    bundleHembras.putBoolean("configurado_hembra", pedido.isConfigurado_hembra());
    bundleHembras.putString("pedidoId", pedido.getPedidoId());
    bundleHembras.putBoolean("tara_finalizada_hembra", pedido.isTara_finalizada_hembra());
    bundleHembras.putBoolean("ave_finalizada", pedido.isAve_finalizada_hembra());

    EdicionPolloMachoFragment edicionPolloMachoFragment = new EdicionPolloMachoFragment();
    edicionPolloMachoFragment.setArguments(bundleMachos);

    EdicionPolloHembraFragment edicionPolloHembraFragment = new EdicionPolloHembraFragment();
    edicionPolloHembraFragment.setArguments(bundleHembras);

    viewPagerAdapter.agregarFragment(edicionPolloMachoFragment, "Macho");
    viewPagerAdapter.agregarFragment(edicionPolloHembraFragment, "Hembra");

    viewPager.setAdapter(viewPagerAdapter);
  }

  private void initActions() {
    btnHoraInicio.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        openTimePickerInicio();
      }
    });

    btnHoraFinal.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        openTimePickerFin();
      }
    });
  }

  private void updateHoraInicio() {
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm aa", Locale.US);
    String shortTimeStr = sdf.format(dateTime.getTime());
    btnHoraInicio.setText(shortTimeStr);
  }

  private void updateHoraFin() {
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm aa", Locale.US);
    String shortTimeStr = sdf.format(dateTime.getTime());
    btnHoraFinal.setText(shortTimeStr);
  }

  private void openTimePickerInicio() {
    new TimePickerDialog(this, horaInicio, dateTime.get(Calendar.HOUR_OF_DAY),
        dateTime.get(Calendar.MINUTE), true).show();
  }

  private void openTimePickerFin() {
    new TimePickerDialog(this, horaFin, dateTime.get(Calendar.HOUR_OF_DAY),
        dateTime.get(Calendar.MINUTE), true).show();
  }
}
