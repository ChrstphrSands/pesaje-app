package com.example.pesajeapp.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.adapters.PesajeAdapter;
import com.example.pesajeapp.database.PesajeDatabse;
import com.example.pesajeapp.model.Pesada;

import java.util.List;

public class PesajeTarasHembraListaActivity extends AppCompatActivity {

  Toolbar toolbar;
  RecyclerView rvPesaje;
  PesajeAdapter pesajeAdapter;

  List<Pesada> pesadas;

  String pedidoId;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_pesaje_taras);
    pedidoId = getIntent().getStringExtra("pedidoId");
    initComponents();
    initUI();
    SelectPesadas selectPesadas = new SelectPesadas();
    selectPesadas.execute();
  }

  private void initComponents() {
    toolbar = findViewById(R.id.toolbar);
    rvPesaje = findViewById(R.id.rv_pesaje);
  }

  private void initUI() {
    setupToolbar();
  }

  private void setupToolbar() {
    Toolbar toolbar = findViewById(R.id.toolbar);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      toolbar.setNavigationIcon(R.drawable.ic_menu_24);
      toolbar.setTitle("LISTA DE PESAJE DE TARAS");
    }

    try {
      toolbar.setTitleTextColor(getResources().getColor(R.color.md_white_1000));
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "No se puede cambiar el color del titulo.");
    }

    try {
      setSupportActionBar(toolbar);
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar el action bar");
    }

    try {
      if (getSupportActionBar() != null) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      }
    } catch (Exception e) {
      Log.e("MAIN ACTIVITY", "Error al configurar la pantalla de inicio como activada");
    }
  }

  private void setupRecyclerView(List<Pesada> pesadas) {
    rvPesaje.setLayoutManager(new LinearLayoutManager(this));

    pesajeAdapter = new PesajeAdapter(pesadas, new PesajeAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(Pesada pesada) {
        startActivity(new Intent(getApplicationContext(), PesajeTaraActivity.class).putExtra(
            "pesadaId", pesada.getPesadaId()));
      }
    });

    rvPesaje.setAdapter(pesajeAdapter);
  }

  private class SelectPesadas extends AsyncTask<Void, Void, List<Pesada>> {

    @Override
    protected List<Pesada> doInBackground(Void... voids) {
      pesadas = PesajeDatabse.getInstance(getApplication()).pesadaStore().getPesadas(pedidoId, "F");
      return pesadas;
    }

    @Override
    protected void onPostExecute(List<Pesada> pesadas) {
      setupRecyclerView(pesadas);
      Log.d("PESADA", pesadas.toString());
    }
  }

}
