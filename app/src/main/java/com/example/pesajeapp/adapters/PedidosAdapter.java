package com.example.pesajeapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.model.Pedido;

import java.util.List;

public class PedidosAdapter extends RecyclerView.Adapter<PedidosAdapter.ViewHolder> {

  public interface OnItemClickListener {
    void onItemClick(Pedido pedido);
  }

  private List<Pedido> pedidos;
  private OnItemClickListener listener;

  public PedidosAdapter(List<Pedido> pedidos, OnItemClickListener listener) {
    this.pedidos = pedidos;
    this.listener = listener;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());

    View view = inflater.inflate(R.layout.item_pedido, parent, false);

    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    Pedido pedido = pedidos.get(position);

    holder.bind(pedido, listener);

    holder.tvClientePedido.setText(pedido.getNombre_cliente());
    holder.tvRucPedido.setText(pedido.getRuc_cliente());
    holder.tvTipoPesajePedido.setText(pedido.getTipo_pedido());
    holder.tvFechaPedido.setText(pedido.getFecha_pedido());

    if (pedido.isConfigurado_macho() || pedido.isConfigurado_hembra()) {
      holder.tvEstadoPedido.setText("En proceso");
      holder.tvEstadoPedido.setTextColor(ContextCompat.getColor(holder.tvEstadoPedido.getContext(), R.color.md_red_400));
      holder.ivImpresionGuia.setColorFilter(ContextCompat.getColor(holder.ivImpresionGuia.getContext(),
          R.color.md_red_400));
    }
    if (pedido.isFinalizado()) {
      holder.tvEstadoPedido.setText("Finalizado");
      holder.tvEstadoPedido.setTextColor(ContextCompat.getColor(holder.tvEstadoPedido.getContext(), R.color.md_green_400));
      holder.ivImpresionGuia.setColorFilter(ContextCompat.getColor(holder.ivImpresionGuia.getContext(), R.color.md_green_400));
    }

  }

  @Override
  public int getItemCount() {
    return pedidos.size();
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    TextView tvClientePedido;
    TextView tvRucPedido;
    TextView tvTipoPesajePedido;
    TextView tvFechaPedido;
    TextView tvEstadoPedido;
    ImageView ivImpresionGuia;

    ViewHolder(View itemView) {
      super(itemView);
      tvClientePedido = itemView.findViewById(R.id.tv_cliente_pedido);
      tvRucPedido = itemView.findViewById(R.id.tv_cliente_edicion);
      tvTipoPesajePedido = itemView.findViewById(R.id.tv_chofer_edicion);
      tvFechaPedido = itemView.findViewById(R.id.tv_placa_edicion);
      tvEstadoPedido = itemView.findViewById(R.id.tv_estado_pedido);
      ivImpresionGuia = itemView.findViewById(R.id.iv_impresion_guia);
    }

    public void bind(final Pedido pedido, final OnItemClickListener listener) {
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          listener.onItemClick(pedido);
        }
      });
    }
  }
}
