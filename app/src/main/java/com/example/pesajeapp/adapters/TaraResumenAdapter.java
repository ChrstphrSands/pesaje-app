package com.example.pesajeapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.model.Pesada;

import java.util.List;

public class TaraResumenAdapter extends RecyclerView.Adapter<TaraResumenAdapter.ViewHolder> {

  private List<Pesada> pesadas;

  public TaraResumenAdapter(List<Pesada> pesadas) {
    this.pesadas = pesadas;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());

    View view = inflater.inflate(R.layout.item_resumen_tara_macho, parent, false);

    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    Pesada pesada = pesadas.get(position);

    holder.tvNroTara.setText(String.valueOf(pesada.getPesadaId()));
    holder.tvNroJaba.setText(String.valueOf(pesada.getJabas_por_pesada()));
    holder.tvNroGalpon.setText(String.valueOf(pesada.getGalpon()));
    holder.tvPeso.setText(String.valueOf(pesada.getTara_peso()));

  }

  @Override
  public int getItemCount() {
    return pesadas.size();
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    TextView tvNroTara;
    TextView tvNroJaba;
    TextView tvNroGalpon;
    TextView tvPeso;

    ViewHolder(View itemView) {
      super(itemView);
      tvNroTara = itemView.findViewById(R.id.tv_nro_tara);
      tvNroJaba = itemView.findViewById(R.id.tv_nro_jaba);
      tvNroGalpon = itemView.findViewById(R.id.tv_nro_galpon);
      tvPeso = itemView.findViewById(R.id.tv_peso);
    }
  }
}
