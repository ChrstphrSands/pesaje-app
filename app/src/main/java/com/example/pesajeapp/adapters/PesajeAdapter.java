package com.example.pesajeapp.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.model.Pesada;

import java.util.List;


public class PesajeAdapter extends RecyclerView.Adapter<PesajeAdapter.PesajeViewHolder> {

  public interface OnItemClickListener {
    void onItemClick(Pesada pesada);
  }

  private List<Pesada> pesadas;
  private OnItemClickListener listener;

  public PesajeAdapter(List<Pesada> pesadas, OnItemClickListener listener) {
    this.pesadas = pesadas;
    this.listener = listener;
  }

  @NonNull
  @Override
  public PesajeViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                             int viewType) {
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    View view = inflater.inflate(R.layout.item_pesada_tara_macho, parent, false);

    return new PesajeViewHolder(view);
  }

  @SuppressLint("DefaultLocale")
  @Override
  public void onBindViewHolder(@NonNull PesajeViewHolder holder, int position) {

    Pesada pesada = pesadas.get(position);

    holder.bind(pesada, listener);

    if (pesada.isTara_pesada()) {
      holder.ivProcesoTara.setImageResource(R.drawable.ic_terminado_24dp);
      holder.ivProcesoTara.setColorFilter(ContextCompat.getColor(holder.ivProcesoTara.getContext(), R.color.md_green_400));
      holder.ivImpresionTara.setColorFilter(ContextCompat.getColor(holder.ivImpresionTara.getContext(),R.color.md_green_400));
    }

    holder.tvNroTara.setText(String.format("Pesaje Tara: %d", position + 1));
  }

  @Override
  public int getItemCount() {
    return pesadas.size();
  }


  class PesajeViewHolder extends RecyclerView.ViewHolder {
    ImageView ivProcesoTara;
    TextView tvNroTara;
    ImageView ivImpresionTara;

    PesajeViewHolder(View itemView) {
      super(itemView);
      ivProcesoTara = itemView.findViewById(R.id.iv_proceso_tara);
      tvNroTara = itemView.findViewById(R.id.tv_nro_tara);
      ivImpresionTara = itemView.findViewById(R.id.iv_impresion_tara);
    }


    void bind(final Pesada pesada, final OnItemClickListener listener) {
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          listener.onItemClick(pesada);
        }
      });
    }
  }

}
