package com.example.pesajeapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.model.ResumenData;

import java.util.List;

public class ResumenesAdapter extends RecyclerView.Adapter<ResumenesAdapter.ViewHolder> {

  public interface OnItemClickListener {
    void onItemClick(ResumenData resumenData);
  }

  private List<ResumenData> resumenDataList;
  private OnItemClickListener listener;

  public ResumenesAdapter(List<ResumenData> resumenDataList, OnItemClickListener listener) {
    this.resumenDataList = resumenDataList;
    this.listener = listener;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.item_resumen, parent, false);

    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    ResumenData sincronizado = resumenDataList.get(position);

    if (position == 0) {
      holder.bind(sincronizado, listener);
    }

    holder.tvCantidad.setText(String.valueOf(sincronizado.getCantidad()));
    holder.tvDescripcion.setText(sincronizado.getDescripcion());
  }

  @Override
  public int getItemCount() {
    return resumenDataList.size();
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    TextView tvCantidad;
    TextView tvDescripcion;

    ViewHolder(View itemView) {
      super(itemView);
      this.tvCantidad = itemView.findViewById(R.id.tv_cantidad_machos);
      this.tvDescripcion = itemView.findViewById(R.id.tv_descripcion);
    }

    void bind(final ResumenData resumenData, final OnItemClickListener listener) {
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onItemClick(resumenData);
        }
      });
    }
  }
}
