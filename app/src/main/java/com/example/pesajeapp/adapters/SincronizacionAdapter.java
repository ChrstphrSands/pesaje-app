package com.example.pesajeapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pesajeapp.R;
import com.example.pesajeapp.model.Sincronizacion;

import java.util.List;

public class SincronizacionAdapter extends RecyclerView.Adapter<SincronizacionAdapter.ViewHolder> {

  public interface OnItemClickListener {
    void onItemClick(Sincronizacion sincronizacion);
  }

  private List<Sincronizacion> sincronizacionList;
  private OnItemClickListener listener;

  public SincronizacionAdapter(List<Sincronizacion> sincronizacionList, OnItemClickListener listener) {
    this.sincronizacionList = sincronizacionList;
    this.listener = listener;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    View view = layoutInflater.inflate(R.layout.item_sincronizacion, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    Sincronizacion sincronizacion = sincronizacionList.get(position);

    holder.bind(sincronizacion, listener);

    holder.tvDescripcion.setText(sincronizacion.descripcion);
  }

  @Override
  public int getItemCount() {
    return sincronizacionList.size();
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    TextView tvDescripcion;

    public ViewHolder(View itemView) {
      super(itemView);
      this.tvDescripcion = itemView.findViewById(R.id.tv_descripcion);
    }

    void bind(final Sincronizacion sincronizacion, final OnItemClickListener listener) {
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          listener.onItemClick(sincronizacion);
        }
      });
    }
  }
}
