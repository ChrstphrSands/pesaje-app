package com.example.pesajeapp.stores;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.pesajeapp.model.Pedido;
import com.example.pesajeapp.model.Usuario;

@Dao
public interface UsuarioStore {

  @Insert
  void insertUsuario(Usuario usuario);

  @Query("select * from usuario where usuario = :usuario and password = :password")
  Usuario login(String usuario, String password);
}
