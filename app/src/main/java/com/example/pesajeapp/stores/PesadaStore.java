package com.example.pesajeapp.stores;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.pesajeapp.model.Pedido;
import com.example.pesajeapp.model.Pesada;

import java.util.List;

@Dao
public interface PesadaStore {

  @Query("SELECT * FROM pesada WHERE pedidoId = :pedidoId AND sexo = :sexo")
  List<Pesada> getPesadas(String pedidoId, String sexo);

  @Insert
  void insertPesada(Pesada pesada);

  @Insert()
  void insertPesadas(List<Pesada> pesadas);

  @Query("SELECT * FROM pesada WHERE pesadaId = :id")
  Pesada getPesada(int id);

  @Query("UPDATE pesada SET tara_peso = :taraPeso, codigo_balanza = :codigoBalanza , galpon = :galpon, " +
      "tara_pesada = 1 WHERE " +
      "pesadaId = " +
      ":pesadaId")
  void updatePesoTara(Double taraPeso, String codigoBalanza, int galpon, int pesadaId);

  @Query("UPDATE pesada SET ave_peso = :avePeso, codigo_balanza = :codigoBalanza, galpon = " +
      ":galpon, ave_pesada = 1 WHERE " +
      "pesadaId = " +
      ":pesadaId")
  void updatePesoAve(Double avePeso, String codigoBalanza, int galpon, int pesadaId);

  @Update
  void updatePesada(Pesada pesada);

  @Delete
  void deletePesada(Pesada pesada);


}
