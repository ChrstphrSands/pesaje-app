package com.example.pesajeapp.stores;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.pesajeapp.model.Galpon;
import com.example.pesajeapp.model.Pedido;
import com.example.pesajeapp.model.PesoResumen;
import com.example.pesajeapp.model.Resumen;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface PedidoStore {
  @Query("Select * from pedido")
  List<Pedido> getPedidos();

  @Insert
  void insertPedido(Pedido pedido);

  @Insert()
  void insertPedidos(List<Pedido> pedidos);

  @Query("SELECT * FROM pedido where pedidoId = :id")
  Pedido getPedido(String id);

  @Update
  void updatePedido(Pedido pedido);

  @Delete
  void deletePedido(Pedido pedido);

  @Query("UPDATE pedido SET configurado_macho = 1 WHERE pedidoId = :id")
  void updatePedidoConfiguradoMacho(String id);

  @Query("UPDATE PEDIDO SET configurado_hembra = 1 WHERE pedidoId = :id")
  void updatePedidoConfiguradoHembra(String id);

  @Query("UPDATE pedido SET tara_finalizada_macho = 1 WHERE pedidoId = :id")
  void cerrarPesajeTarasMacho(String id);

  @Query("UPDATE pedido SET tara_finalizada_hembra = 1 WHERE pedidoId = :id")
  void cerrarPesajeTarasHembra(String id);

  @Query("UPDATE pedido SET ave_finalizada_macho = 1 WHERE pedidoId = :id")
  void cerrarPesajeAvesMacho(String id);

  @Query("UPDATE pedido SET ave_finalizada_hembra = 1 WHERE pedidoId = :id")
  void cerrarPesajeAvesHembra(String id);

  @Query("SELECT COUNT(ps.pesadaId) as cantidad_pesadas, (COUNT(ps.pesadaId) * jabas_por_pesada) " +
      "as jaba_pesada, aves_por_jaba as aves_jaba FROM pedido as p inner join pesada as ps on ps" +
      ".pedidoId = p.pedidoId where ps.pedidoId = :id and ps.sexo = :sexo")
  PesoResumen getPesoResumen(String id, String sexo);

  @Query("SELECT p.cantidad_machos AS cantidad_aves, ps.jabas_por_pesada AS cantidad_jabas, " +
      "SUM(ps.tara_peso) AS total_peso_taras, SUM(ps.ave_peso) AS peso_bruto, " +
      "(SUM(ps.tara_peso) - SUM(ps.ave_peso)) AS peso_neto, AVG (ps.ave_peso) AS " +
      "peso_promedio, ps.aves_por_jaba , ps.galpon AS galpon, ps.codigo_balanza as codigo_balanza" +
      " FROM pedido AS p" +
      " INNER JOIN pesada AS ps ON p.pedidoId = ps.pedidoId WHERE ps.pedidoId = :id AND ps.sexo = :sexo")
  Resumen getResumenMacho(String id, String sexo);

  @Query("SELECT DISTINCT(ps.galpon) AS galpon" +
      " FROM pedido AS p" +
      " INNER JOIN pesada AS ps ON p.pedidoId = ps.pedidoId WHERE ps.pedidoId = :id AND ps.sexo = :sexo")
  List<Galpon> getGalpones(String id, String sexo);

  @Query("select count(pedidoId) as cantidad from pedido")
  int getTotalPedidos();

  @Query("select COUNT(pedidoId) as cantidad from pedido where configurado_hembra = 1 or configurado_macho = 1")
  int getRealizados();

  @Query("select count(pedidoId) as cantidad from pedido where finalizado = 1")
  int getCerrados();

}
